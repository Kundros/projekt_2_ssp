from django.contrib import admin

# Register your models here.
from projekt import models

admin.site.register(models.User)
admin.site.register(models.MatchPlayers)
admin.site.register(models.Match)
admin.site.register(models.Round)
admin.site.register(models.Assist)
admin.site.register(models.Kill)
admin.site.register(models.Inventory)
admin.site.register(models.Purchase)
