import locale

from django.shortcuts import render, redirect, get_object_or_404
from .forms import Login, Register, CommentCreate, Search, AddMatch, DeleteMatch
from .models import Match, MatchPlayers, User, Round, Kill, Assist, Purchase, Inventory, Comments

# Create your views here.
def index(request):
    return render(request, 'projekt/index.html')


def login(request):
    if request.method == 'POST':
        form = Login(request.POST)
        if form.is_valid():
            user = form.login()

            if user is not None:
                request.session['user_id'] = user.pk
                request.session['is_admin'] = user.role == 'Admin'
                return redirect('index')
            else:
                form.add_error(None, 'Invalid login')

    return render(request, 'projekt/login.html', {"hide_nav": True})


def register(request):
    if request.method == 'POST':
        form = Register(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = Register()

    return render(request, 'projekt/register.html', {
        "hide_nav": True,
        "form": form
    })


def logout(request):
    del request.session["user_id"]
    del request.session["is_admin"]
    return redirect("index")


def matches(request):
    if not request.session.get("user_id"):
        return redirect('index')

    _matches = MatchPlayers.objects.filter(
        user=User.objects.get(pk=request.session["user_id"])
    )

    return render(request, 'projekt/game/matches.html', {"matches": _matches})


def matches_id(request, id):
    if not request.session.get("user_id"):
        return redirect('index')

    if request.method == 'POST':
        form = CommentCreate(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)

            comment.related_to = "match"
            comment.related_id = id
            comment.user = User.objects.get(pk=request.session["user_id"])

            comment.save()

            return redirect('matches_id', id)

    _matches = MatchPlayers.objects.filter(
        user=User.objects.get(pk=request.session["user_id"])
    )

    try:
        display_match = _matches.filter(
            match_id=id
        )[0]

        rounds = Round.objects.filter(
            match_id=id
        )

        table_kills = Kill.objects.all()

        kills = table_kills.filter(
            killer_id=request.session["user_id"],
            round_id__in=rounds
        )

        deaths = table_kills.filter(
            victim_id=request.session["user_id"],
            round_id__in=rounds
        )

        assists = Assist.objects.filter(
            assistant_id=request.session["user_id"],
            round_id__in=rounds
        )

        comments = Comments.objects.filter(
            related_to="match",
            related_id=id
        )

        for round in rounds:
            round.kills = kills.filter(round_id=round.pk)

        won_rounds = rounds.filter(won=display_match.team).count()

    except:
        return redirect('matches')

    return render(request, 'projekt/game/matches.html', {
        "matches": _matches,
        "display_match": display_match,
        "rounds_att": rounds[0:15],
        "rounds_def": rounds[16:30],
        "kills": kills,
        "deaths": deaths,
        "assists": assists,
        "won_rounds": won_rounds,
        "rest_attackers": 15 - rounds.count(),
        "rest_defenders": 15 if 30 - rounds.count() > 15 else 30 - rounds.count(),
        "comments": comments,
        "form": CommentCreate()
    })


def rounds_id(request, id):
    if not request.session.get("user_id"):
        return redirect('index')

    try:
        _round = Round.objects.get(pk=id)
        match = _round.match
        match_players = MatchPlayers.objects.all().filter(match=match)
        team1 = match_players.filter(team=True)
        team2 = match_players.filter(team=False)

        kills = Kill.objects.filter(
            round_id=id
        )

        assists = Assist.objects.filter(
            round_id=id
        )

        purchases = Purchase.objects.filter(
            round_id=id
        )

        for player in team1:
            player.kills = kills.filter(killer_id=player.user.pk)
            player.death = kills.filter(victim_id=player.user.pk).first()

        for player in team2:
            player.kills = kills.filter(killer_id=player.user.pk)
            player.death = kills.filter(victim_id=player.user.pk).first()

    except:
        return redirect('matches')

    return render(request, 'projekt/game/nav_ignore/rounds.html', {
        "round": _round,
        "match": match,
        "team1": team1,
        "team2": team2,
        "rest_t1": 5 - team1.count(),
        "rest_t2": 5 - team2.count(),
        "length": _round.end_date - _round.start_date,
        "kills": kills,
        "assists": assists,
        "spent": sum(map(lambda x: x.item.price * x.amount, purchases)),
        "purchases": purchases,
        "purchases_count": sum(map(lambda x: x.amount, purchases))
    })


def items(request):
    try:
        items = Inventory.objects.all().filter(deleted=False)
    except:
        return redirect('index')

    return render(request, 'projekt/game/items.html', {"items": items})


def purchases(request):
    if not request.session.get("user_id"):
        return redirect('index')

    try:
        purchases_ = Purchase.objects.filter(
            user_id=request.session["user_id"],
        )

        matches = {}

        for purchase in purchases_:
            if not matches.get(purchase.round.match.start_date):
                matches[purchase.round.match.start_date] = []
            matches[purchase.round.match.start_date].append(purchase)
    except:
        return redirect('index')

    return render(request, 'projekt/game/purchases.html', {"matches": matches})


def search(request):
    if not request.session.get("user_id"):
        return redirect('index')

    matches_ = []

    if request.method == 'POST':
        form = Search(request.POST)
        if form.is_valid():
            matches_ = form.get_matches()

    return render(request, 'projekt/game/search.html', {
        "form": Search(),
        "matches": matches_
    })


def match_view(request, id):
    if not request.session.get("user_id"):
        return redirect('index')

    if request.method == 'POST':
        form = CommentCreate(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)

            comment.related_to = "match"
            comment.related_id = id
            comment.user = User.objects.get(pk=request.session["user_id"])

            comment.save()

            return redirect('match_view', id)

    try:
        display_match = Match.objects.get(
            pk=id
        )

        rounds = Round.objects.filter(
            match_id=id
        )

        table_kills = Kill.objects.all()

        kills = table_kills.filter(
            round_id__in=rounds
        )
        assists = Assist.objects.filter(
            round_id__in=rounds
        )

        comments = Comments.objects.filter(
            related_to="match",
            related_id=id
        )

        match_players = MatchPlayers.objects.filter(
            match_id=id
        )

        team1 = match_players.filter(team=True)
        team2 = match_players.filter(team=False)

        for round in rounds:
            round.kills = kills.filter(round_id=round.pk)

    except:
        return redirect('index')

    return render(request, 'projekt/game/nav_ignore/match_view.html', {
        "display_match": display_match,
        "kills": kills,
        "assists": assists,
        "comments": comments,
        "match_players": match_players,
        "form": CommentCreate(),
        "team1": team1,
        "team2": team2
    })


def profile(request):
    if not request.session.get("user_id"):
        return redirect('index')

    try:
        user = User.objects.get(
            pk=request.session["user_id"]
        )

        matches_for_player = MatchPlayers.objects.filter(
            user_id=request.session["user_id"]
        )

        player_rounds = Round.objects.filter(
            match__in=matches_for_player.values_list('match')
        )

        player_kills = Kill.objects.filter(
            killer_id=request.session["user_id"]
        )

        all_purchases = Purchase.objects.filter(
            user_id=request.session["user_id"]
        )

        all_spent = 0
        for purchase in all_purchases:
            all_spent += purchase.amount * purchase.item.price
    except:
        return redirect('index')

    locale.setlocale(locale.LC_ALL, '')
    return render(request, 'projekt/profile.html', {
        "user": user,
        "matches": matches_for_player,
        "rounds": player_rounds,
        "kills": player_kills,
        "spent": locale.currency(all_spent, grouping=True)
    })

def add_game(request):
    if not request.session.get("user_id") or not request.session.get("is_admin"):
        return redirect('index')

    if request.method == 'POST':
        form = AddMatch(request.POST)
        if form.is_valid():
            form.save()

            return redirect('add_game')
    else:
        form = AddMatch()

    try:
        matches = Match.objects.all()
    except:
        return redirect('index')

    return render(request, 'projekt/game/admin/add.html', {
        "matches": matches,
        "form": form
    })


def delete_match(request, id):
    if not request.session.get("user_id") or not request.session.get("is_admin"):
        return redirect('index')

    obj = get_object_or_404(Match, id=id)

    if request.method == 'POST':
        form = DeleteMatch(request.POST)
        if form.is_valid():
            if form.cleaned_data['confirmation']:
                obj.delete()

    return redirect("add_game")


def players(request):
    if not request.session.get("user_id"):
        return redirect('index')

    try:
        users = User.objects.all()

        for user in users:
            user.matches = Match.objects.filter(
                matchplayers__user_id=user.pk
            )
    except:
        return redirect('index')

    return render(request, 'projekt/players.html', {
        "users": users
    })