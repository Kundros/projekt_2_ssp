from os import listdir
from os.path import isdir, join, isfile


def project_templates(request):
    project_templates_var = []
    project_templates_path = "templates/projekt"
    for file in listdir(project_templates_path):
        file = file.split('.')[0]
        if isdir(join(project_templates_path, file)) and file not in ["partials"]:
            project_templates_var.append({
                'type': 'array',
                'name': file,
                'items': [
                    {
                        'name': dir_file.split('.')[0],
                        'href': "/" + file + "/" + dir_file.split('.')[0],
                        'admin': False
                    } for dir_file in listdir(join(project_templates_path, file))
                    if isfile(join(project_templates_path, file, dir_file))
                ]
                +
                [
                     {
                         'name': dir_file.split('.')[0],
                         'href': "/" + file + "/" + dir_file.split('.')[0],
                         'admin': True
                     } for dir_file in listdir(join(project_templates_path, file, 'admin'))
                     if isfile(join(project_templates_path, file, 'admin', dir_file))
                 ]
            })
        elif file not in ["layout", "partials", "register", "login", "index"]:
            project_templates_var.append({
                'type': 'item',
                'name': file,
                'href': "/" + file
            })

    return {"project_templates": project_templates_var}


def get_user_session(request):
    return {"user_session": request.session.get('user_id') }
