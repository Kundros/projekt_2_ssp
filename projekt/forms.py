from .models import User, Comments, Match
from django import forms

class Login(forms.Form):
    nickname = forms.CharField(max_length=32, label='nickname')
    password = forms.CharField(max_length=256, label='Password')

    def clean(self):
        cleaned_data = super().clean()
        nickname = cleaned_data.get('nickname')
        password = cleaned_data.get('password')

        try:
            user = User.objects.get(nickname=nickname)
        except User.DoesNotExist:
            raise forms.ValidationError('Nickname is invalid')

        if not user.valid_password(password):
            raise forms.ValidationError('Password is invalid')

        cleaned_data['user'] = user
        return cleaned_data

    def login(self):
        return self.clean()['user']


class Register(forms.ModelForm):
    password_repeat = forms.CharField(max_length=256, label='Repeat password')

    class Meta:
        model = User
        fields = ['nickname', 'email', 'password']

    def clean_nickname(self):
        if User.objects.get(nickname=self.cleaned_data['nickname']) is not None:
            raise forms.ValidationError("Nickname is already used.")

        return self.cleaned_data['nickname']

    def clean_password_repeat(self):
        password = self.cleaned_data['password']
        password_repeat = self.cleaned_data['password_repeat']

        if password != password_repeat:
            raise forms.ValidationError("Passwords are not same.")

        return password_repeat

    def save(self, commit=True):
        user = super().save(commit=False)
        user.save_password(self.cleaned_data['password_repeat'])

        if commit:
            user.save()

        return user


class CommentCreate(forms.ModelForm):
    class Meta:
        model = Comments
        fields = ['message']


    def clean_message(self):
        message = self.cleaned_data['message']

        if len(message) < 5:
            raise forms.ValidationError("Length must be at least 5 characters long.")

        return message

    def save(self, commit=True):
        comment = super().save(commit=False)

        if commit:
            comment.save()

        return comment


class Search(forms.Form):
    search = forms.DateField(label='search')

    def clean(self):
        cleaned_data = super().clean()
        search = cleaned_data.get('search')

        matches = Match.objects.filter(start_date__date=search)

        cleaned_data['matches'] = matches
        return cleaned_data

    def get_matches(self):
        return self.clean()['matches']


class AddMatch(forms.ModelForm):
    class Meta:
        model = Match
        fields = ["won", "start_date", "end_date"]

    def clean_end_date(self):
        cleaned_data = super().clean()

        if cleaned_data["start_date"] > cleaned_data["end_date"]:
            raise forms.ValidationError("End date must be after start date.")

        return cleaned_data["end_date"]


class DeleteMatch(forms.ModelForm):
    class Meta:
        model = Match
        fields = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['confirmation'].required = True
        self.fields['confirmation'].widget = forms.HiddenInput()

    confirmation = forms.BooleanField(label='Are you sure you want to delete?', required=False)

    def clean_confirmation(self):
        if not self.cleaned_data['confirmation']:
            raise forms.ValidationError("Please confirm that you want to delete this object.")
        return self.cleaned_data['confirmation']
