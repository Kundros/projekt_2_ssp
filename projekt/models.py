from datetime import datetime
from django.db import models
from django.contrib.auth.hashers import make_password, check_password

# Create your models here.
rank_choices = [
    ("UN", "Unranked"),
    ("I1", "Iron I"), ("I2", "Iron II"), ("I3", "Iron III"), ("I4", "Iron IV"),
    ("B1", "Bronze I"), ("B2", "Bronze II"), ("B3", "Bronze III"), ("B4", "Bronze IV"),
    ("S1", "Silver I"), ("S2", "Silver II"), ("S3", "Silver III"), ("S4", "Silver IV"),
    ("G1", "Gold I"), ("G2", "Gold II"), ("G3", "Gold III"), ("G4", "Gold IV"),
    ("P1", "Platinum I"), ("P2", "Platinum II"), ("P3", "Platinum III"), ("P4", "Platinum IV"),
    ("D1", "Diamond I"), ("D2", "Diamond II"), ("D3", "Diamond III"), ("D4", "Diamond IV"),
    ("U1", "Ultimate I"), ("U2", "Ultimate II"), ("U3", "Ultimate III"),
    ("H0", "Hero")
]


class User(models.Model):
    nickname = models.CharField(max_length=32, null=False)
    role = models.CharField(max_length=32, null=False, default="User", choices=[("User", "User"), ("Admin", "Admin")])
    rank = models.CharField(max_length=2, null=False, default="UN", choices=rank_choices)
    password = models.CharField(max_length=256, null=False)
    email = models.CharField(max_length=128, null=False)
    last_logged_in = models.DateTimeField(null=False, default=datetime.now)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return f"Id: {self.pk} || {self.nickname} | {self.role} | {self.email} ||"

    def save_password(self, password):
        self.password = make_password(password)

    def valid_password(self, password):
        return check_password(password, self.password)


class Match(models.Model):
    won = models.BooleanField(null=False, default=0)
    start_date = models.DateTimeField(null=False, default=datetime.now)
    end_date = models.DateTimeField(null=True)

    def __str__(self):
        return f"Id: {self.pk} || Won: {'team 1' if self.won else 'team 2'} | Duration: {self.end_date - self.start_date} ||"


class MatchPlayers(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    match = models.ForeignKey(Match, on_delete=models.CASCADE, null=False)
    team = models.BooleanField(null=False, default=True)
    bullets_shot = models.IntegerField(null=False, default=0)
    bullets_hit = models.IntegerField(null=False, default=0)
    match_rank = models.CharField(max_length=2, null=False, choices=rank_choices)

    def __str__(self):
        return f"Id: {self.pk} || User_id: {self.user.pk} | Match_id: {self.match.pk} ||"


class Round(models.Model):
    match = models.ForeignKey(Match, on_delete=models.CASCADE, null=False)
    won = models.BooleanField(null=False, default=0)
    start_date = models.DateTimeField(null=False, default=datetime.now)
    end_date = models.DateTimeField(null=True)

    def __str__(self):
        return f"Id: {self.pk} || Match_id: {self.match.pk} | Won: {'team 1' if self.won else 'team 2'} | Duration: {self.end_date - self.start_date} ||"


class Assist(models.Model):
    round = models.ForeignKey(Round, on_delete=models.CASCADE, null=False)
    assistant = models.ForeignKey(User, on_delete=models.CASCADE, null=False, related_name='assistant')
    victim = models.ForeignKey(User, on_delete=models.CASCADE, null=False, related_name='victim2')
    damage = models.IntegerField(null=False, default=50)

    def __str__(self):
        return f"Id: {self.pk} || Round_id: {self.round.pk} | Assistant: {self.assistant.nickname} | Victim: {self.victim.nickname} | Demage: {self.damage} ||"


class Inventory(models.Model):
    name = models.CharField(max_length=32, null=False)
    price = models.IntegerField(default=100, null=False)
    type = models.CharField(max_length=2, null=False)
    description = models.CharField(max_length=256, null=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return f"Id: {self.pk} || {self.name} | {self.price} | {self.type} | {self.description} | {self.deleted} ||"


class Kill(models.Model):
    round = models.ForeignKey(Round, on_delete=models.CASCADE, null=False)
    killer = models.ForeignKey(User, on_delete=models.CASCADE, null=False, related_name='killer')
    victim = models.ForeignKey(User, on_delete=models.CASCADE, null=False, related_name='victim1')
    item = models.ForeignKey(Inventory, on_delete=models.CASCADE, null=True)
    damage = models.IntegerField(null=False, default=1)

    def __str__(self):
        return f"Id: {self.pk} || Round_id: {self.round.pk} | Killer: {self.killer.nickname} | Victim: {self.victim.nickname} | Demage: {self.damage} ||"


class Purchase(models.Model):
    round = models.ForeignKey(Round, on_delete=models.CASCADE, null=False)
    item = models.ForeignKey(Inventory, on_delete=models.CASCADE, null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    amount = models.IntegerField(default=1, null=False)

    def __str__(self):
        return f"Id: {self.pk} || Round_id: {self.round.pk} | Item: {self.item.name} | Player: {self.user.nickname} | Amount: {self.amount} ||"


class Comments(models.Model):
    related_to = models.CharField(max_length=32, null=False)
    related_id = models.IntegerField(null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    message = models.TextField(null=False)

    def __str__(self):
        return f"Id: {self.pk} || {self.related_to} | {self.user} | {self.message} ||"

