"""
URL configuration for skjProjekt projekt.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from projekt import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('logout/', views.logout, name='logout'),
    path('game/matches', views.matches, name='matches'),
    path('game/matches/<int:id>', views.matches_id, name='matches_id'),
    path('game/rounds/<int:id>', views.rounds_id, name='rounds_id'),
    path('game/items', views.items, name='items'),
    path('game/purchases', views.purchases, name='purchases'),
    path('game/search', views.search, name='search'),
    path('game/match_view/<int:id>', views.match_view, name='match_view'),
    path('profile', views.profile, name='profile'),
    path('game/add', views.add_game, name='add_game'),
    path('game/delete/<int:id>', views.delete_match, name='delete_match'),
    path('players', views.players, name='players')
]
